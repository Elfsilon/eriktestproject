//Даны три числа.
//Определите, можно ли из отрезков с такими длинами составить треугольник.
//Определите вид треугольника (прямоугольный, тупоугольный, остроугольный),
//если он существует.
//Даны числа min и max.
//Найдите все треугольники с целочисленными длинами сторон от min до max включительно.
//Напишите программу, которая определяет, можно ли из четырех отрезков с данными длинами
//a, b, c и d составить прямоугольник.

import 'dart:math' as math;

class Triangle {
  int x;
  int y;
  int z;

  Triangle(this.x, this.y, this.z);

  Map<String, dynamic> _listSqure(int x, int y, int z) {
    var squares = Map<String, dynamic>();
    if (x < z && y < z) {
      squares['sumOfSmallSid'] = math.pow(x, 2) + math.pow(y, 2);
      squares['bigSide'] = math.pow(z, 2);
    }
    if (z < x && y < x) {
      squares['sumOfSmallSid'] = math.pow(z, 2) + math.pow(y, 2);
      squares['bigSide'] = math.pow(x, 2);
    }
    if (z < y && x < y) {
      squares['sumOfSmallSid'] = math.pow(z, 2) + math.pow(x, 2);
      squares['bigSide'] = math.pow(y, 2);
    }
    if ((x == y) && (y == z)) {
      squares['sumOfSmallSid'] = math.pow(z, 2) + math.pow(x, 2);
      squares['bigSide'] = math.pow(y, 2);
    }
    return squares;
  }

  String triangleIsExist() {
    if (x == 0 || y == 0 || z == 0) {
      throw 'такого треугольника не существует';
    }
    if (x > y + z || z > x + y || y > x + z) {
      return 'noExist';
    } else {
      return 'exist';
    }
  }

  String typeOfTriangle() {
    Map squires = _listSqure(x, y, z);

    if (squires['bigSide'] == squires['sumOfSmallSid']) {
      return 'прямоугольный';
    }
    if (squires['bigSide'] < squires['sumOfSmallSid']) {
      return 'остроугольный';
    }
    if (squires['bigSide'] > squires['sumOfSmallSid']) {
      return 'тупоугольный';
    }
    return "";
  }
}

void main() {
  var trian = Triangle(1, 1, 1);

  print(trian.typeOfTriangle());
  print(trian.triangleIsExist());
}
