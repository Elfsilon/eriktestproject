// хочешь еще круче задание? то же самое сделай только вместо
/// стандартных типов будут классы
/// типо есть класс Data, у которого есть поле int value, и наши
/// списки будут не List<int> а List<Data>
/// (это типичная ситуация в реальности)

class Digit {
  const Digit(this.value);

  static List<Digit> fromList(List<int> lst) {
    return lst.map((e) => Digit(e)).toList();
  }

  final int value;
}

void t() {
  final List<Digit> lst = Digit.fromList([3, 5, 9, 1, 4, 2, 3]);
  final result = lst.where((element) => element.value % 3 == 0);
  print(result);
}

/// и попробуй удалить все повторения элементов в списке
/// (именно со своим классом Data)
/// 1. используя Set
/// 2. Используя List (используя data1 == data2)
/// можешь еще попробовать через Map если получится
/// за подсказкой можешь прийти подскажу)

void tt() {
  final Set<Digit> s = {
    Digit(3),
    Digit(3),
    Digit(9),
    Digit(1),
    Digit(4),
    Digit(2),
    Digit(3),
  };

  print(s.length);
}
